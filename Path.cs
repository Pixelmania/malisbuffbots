﻿namespace MalisBuffBots
{
    public class Path
    {
        public static string PLUGIN_DIR;
        public static string SETTINGS_JSON;
        public static string BUFF_JSON;
        public static string REBUFF_JSON;
        public static string USERRANK_JSON;
        public static string BAN_JSON;

        public static void Init(string pluginDir)
        {
            PLUGIN_DIR = pluginDir;
            SETTINGS_JSON =  $"{PLUGIN_DIR}\\JSON\\Settings.json";
            BUFF_JSON =  $"{PLUGIN_DIR}\\JSON\\BuffsDb.json";
            REBUFF_JSON =  $"{PLUGIN_DIR}\\JSON\\RebuffInfo.json";
            USERRANK_JSON =  $"{PLUGIN_DIR}\\JSON\\UserRanks.json";
            BAN_JSON =  $"{PLUGIN_DIR}\\JSON\\BanList.json";
        }
    }
}

